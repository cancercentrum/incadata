hexSticker::sticker(
  "hex/INCA_Logo_Stor.png", 
  package   = "incadata", 
  p_size    = 8,
  p_y       = 1.45,
  s_x       = 1, 
  s_y       = .75, 
  s_width   = .5,
  filename  = "hex/hexsticker.png",
  url       = "cancercentrum.bitbucket.io/incadata",
  u_size    = 1.4,
  u_color   = "white",
  h_color   = "black",
  spotlight = TRUE,
  l_x       = 1,
  l_y       = .75
)
